import os
import json
from termcolor import colored
import sys

low_count = 0
medium_count = 0
high_count = 0
critical_count = 0
fixable = []
image = sys.argv[1]


def printTable(myDict, colList=None, sep='\uFFFA'):

    if not colList:
        colList = list(myDict[0].keys() if myDict else [])
    myList = [colList]
    for item in myDict:
        myList.append([str(item[col] or '') for col in colList])
    colSize = [max(map(len, (sep.join(col)).split(sep))) for col in zip(*myList)]
    formatStr = ' | '.join(["{{:<{}}}".format(i) for i in colSize])
    line = formatStr.replace(' | ', '-+-').format(*['-' * i for i in colSize])
    item = myList.pop(0)
    lineDone = False
    while myList or any(item):
        if all(not i for i in item):
            item = myList.pop(0)
            if line and (sep != '\uFFFA' or not lineDone):
                print(line)
                lineDone = True
        row = [i.split(sep, 1) for i in item]
        print(formatStr.format(*[i[0] for i in row]))
        item = [i[1] if len(i) > 1 else '' for i in row]


json_output = os.popen(f'trivy --quiet --timeout 10m -f json {image}').read()

json_result = json.loads(json_output)

for i in json_result['Results']:
    if i['Vulnerabilities']:
        for j in i['Vulnerabilities']:
            if 'FixedVersion' in j:
                fixable.append({'Package Name': j['PkgName'], 'Installed Verson': j['InstalledVersion'], 'Fixed Version': j['FixedVersion'],
                    'Severity': j['Severity'],
                    'CVE Number': j['VulnerabilityID'],
                    'Reference': f"https://nvd.nist.gov/vuln/detail/{j['VulnerabilityID']}"})
            if j['Severity'] == 'LOW':
                low_count += 1
            if j['Severity'] == 'MEDIUM':
                medium_count += 1
            if j['Severity'] == 'HIGH':
                high_count += 1
            if j['Severity'] == 'CRITICAL':
                critical_count += 1
    else:
        print('No vulnerabilities found')
        sys.exit(0)

print("Image vulnerabilities count:\n")
print(colored(f'LOW:{low_count}', 'blue'), colored(f'MEDIUM:{medium_count}', 'green'),
    colored(f'HIGH:{high_count}', 'yellow'), colored(f'CRITICAL:{critical_count}', 'red'))

print("\nFixable packages:\n")

if fixable:
    printTable(fixable)
else:
    print("None of the packages affected are fixed.")